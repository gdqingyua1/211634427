import random
import string
import datetime
import time

random.seed(1)
# 1.构造15个学生键值对{姓名：分数}, 存入学生字典
dct = {}
for i in string.ascii_uppercase[0:15]:
    dct[i] = random.randint(35, 100)
print(dct)

# 2.按照学生成绩降序排列输出字典
a1 = sorted(dct.items(), key=lambda x: x[1], reverse=True)
print(a1)

# 3.计算学生成绩平均分、最大值、最小值
scores = list(dct.values())
print("---", scores)
ave = sum(scores) / len(scores)
max1 = max(scores)
min1 = min(scores)
print('平均分：{:^10.2f}'.format(ave))
print('最大值：{:^10.2f}'.format(max1))
print('最小值：{:^10.2f}'.format(min1))

# 4.统计不及格人数的个数
num = 0
for i in scores:
    if i < 60:
        num = num + 1
print(f"不及格人数：{num}")


# 5.输入学生名字，如W，输出学生对应的分数
def check_score():
    check_num = 0
    while True:
        name = input("请输入要查询的学生姓名：")

        if name not in dct:
            print('请输入正确的学生名字')
            check_num += 1
            continue
        else:
            score = dct[name]
            avg_score = sum(scores) / len(scores)
            if score > avg_score:
                print("该生成绩属于上等水平")
            elif score == avg_score:
                print("该生成绩属于中等水平")
            else:
                print("该生成绩属于下等水平")
            print(f"查询了{check_num + 1}次")
            dtt = datetime.datetime
            print(type(dtt))
            print(dtt.today())
            print(dtt.now())
            import matplotlib.pyplot as plt
            plt.rcParams['font.sans-serif'] = 'simhei'
            label = ['下等', '上等', '中等']
            data = [106, 94, 0]
            plt.pie(data, labels=label, autopct='%.2f%%')
            plt.show()
            break


t = time.time()
check_score()

print('耗时:{:.8f}s'.format(time.time() - t))